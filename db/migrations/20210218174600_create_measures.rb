Sequel.migration do
  up do
    create_table(:measures) do
      primary_key :id

      Integer :recipe_id

      String :ingredient, null: false
      Integer :quantity
      String :unit

      alter_table(:measures) do
        add_foreign_key :recipe_id, :recipes
      end
    end
  end

  down do
    drop_table(:measures)
  end
end
