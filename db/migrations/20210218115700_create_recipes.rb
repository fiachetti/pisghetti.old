Sequel.migration do
  up do
    create_table(:recipes) do
      primary_key :id
      String :title, null: false
      String :slug,  null: false
    end
  end

  down do
    drop_table(:recipes)
  end
end
