module.exports = {
  purge: [
    // './app/javascript/**/*.js',
  ],
  theme: {
    extend: {
      width: {
        '128': '32rem',
      },
      height: {
        '128': '32rem',
      },
      maxWidth: {
        '128': '32rem',
      },
      maxHeight: {
        '128': '32rem',
      },
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
