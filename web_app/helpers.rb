module Helpers
  def link_to(text=nil, url, **html_options, &block)
    element_text    = text || block.call
    html_attributes = html_options.map { |k,v|
      "#{k}=\"#{v}\""
    }.join(" ")

    <<~HTML
      <a href="#{url}" #{html_attributes}>
        #{element_text}
      </a>
    HTML
  end
end
