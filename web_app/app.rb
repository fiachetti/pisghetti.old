require "object"

require "awesome_print"
AwesomePrint.defaults = {
  indent: 2,
  index:  false,
}

puts [
  "Running on the".yellowish,
  ENV.fetch("ENVIRONMENT").green,
  "environment".yellowish,
].join(" ")

require "helpers"

require "environment/environment"
ENVIRONMENT = Environment.current

def deep_symbolize_keys(hash)
  Hash.new.tap { |symbolized_hash|
    hash.each do |k, v|
      symbolized_hash[k.to_sym] = v.is_a?(Hash) ? deep_symbolize_keys(v) : v
    end
  }
end

class App < Roda
  include Helpers

  plugin :render, views: "./web_app/views"

  plugin :h

  plugin :sessions, secret: ENV.fetch('RACK_SECRET')
  plugin :flash

  plugin :public, root: "./web_app/public"
  plugin :partials

  recipe_system ||= ENVIRONMENT.recipe_system

  route do |r|
    puts "  >>>>> #{__FILE__}:#{__LINE__}".purple
    params = deep_symbolize_keys(r.params)
    ap params
    puts "  >>>>> #{__FILE__}:#{__LINE__}".purple

    r.public

    r.root do
      view("recipes", locals: {
             recipes: recipe_system.recipes
           })
    end

    r.on "danger" do
      r.is "reset" do
        recipe_system.reset!

        flash[:danger] = "The system has been reset!"

        r.redirect "/"
      end

      r.is "seed" do
        recipe_system.reset!
        (params[:num_recipes] || 3).times do |recipe_number|
          slug  = "recipe-#{recipe_number}"
          title = "Recipe #{recipe_number}"
          recipe_system.create_recipe(slug: slug, title: title)

          rand(1..(params[:num_measures] || 5)).times do |measure_number|
            recipe_system.add_measure(slug: slug, ingredient: "Ingredient #{measure_number}", quantity: rand(10))
          end

        end

        flash[:danger] = "The system has been seeded!"

        r.redirect "/"
      end
    end

    r.on "recipes" do
      r.root do
        view("recipes", locals: {
               recipes: recipe_system.recipes
             })
      end

      r.is :slug do |slug|
        view("recipe", locals: {
               recipe: recipe_system.find_recipe(slug)
             })
      end

      r.post "add_measure" do
        recipe_system.add_measure(**params.delete_if { |_,v| v.blank? })


        r.redirect "/"
      end

      r.post do
        recipe_system.create_recipe(**params)

        r.redirect "/"
      end

    end
  end
end
