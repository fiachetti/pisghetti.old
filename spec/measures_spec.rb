require "spec_helper"
require "measure"

class MeasuresSpecFactory

end

RSpec.describe "Measures" do
  let(:f) { MeasuresSpecFactory.new }

  describe "measure" do

    it "can't be created without an ingredient" do
      expect {
        Measure.for
      }.to raise_error(ArgumentError, "missing keyword: :ingredient")
    end

    it "can't be created with an empty ingredient" do
      expect {
        Measure.for(ingredient: nil)
      }.to raise_error(ArgumentError, "blank keyword: :ingredient")
    end

    it "can be created without a unit and a quantity" do
      measure = Measure.for(ingredient: "salt")

      expect(measure).to eq(Measure.for(ingredient: "salt"))
      expect(measure.ingredient?("salt")).to eql(true)
      expect(measure.quantity?(Quantity.no_quantity)).to eql(true)
      expect(measure.unit?(Measure::NoUnit)).to eql(true)
    end

    it "can have a quantity of 1" do
      measure = Measure.for(ingredient: "cherry", quantity: 1)

      expect(measure.quantity?(1)).to eql(true)
      expect(measure.unit?(Measure::NoUnit)).to eql(true)
      expect(measure.unit?("kg")).to eql(false)
    end

    it "can have an empty quantity" do
      measure = Measure.for(ingredient: "cherry", quantity: "")

      expect(measure.quantity?(0)).to eql(true)
    end

    it "can have a quantity of more than 1" do
      measure = Measure.for(ingredient: "cherry", quantity: 2)

      expect(measure.quantity?(2)).to eql(true)
      expect(measure.quantity?(1)).to eql(false)
    end

    it "can't have a negative quantity" do
      expect {
        Measure.for(ingredient: "cherry", quantity: -1)
      }.to raise_error(Quantity::NegativeQuantityError)
    end

    it "can't have an alphanumeric quantity" do
      expect {
        Measure.for(ingredient: "cherry", quantity: "1a")
      }.to raise_error(ArgumentError, "invalid value for Integer(): \"1a\"")
    end


    it "can be created with a unit" do
      measure = Measure.for(ingredient: "cherry", unit: "kg")

      expect(measure.unit?("kg")).to eql(true)
      expect(measure.unit?(Measure::NoUnit)).to eql(false)
    end

    describe "operating on measures" do
      it "can't be added if they have different ingredients" do
        measure1 = Measure.for(ingredient: "ingredient 1", quantity: 1, unit: "unit")
        measure2 = Measure.for(ingredient: "ingredient 2", quantity: 1, unit: "unit")

        expect {
          measure1 + measure2
        }.to raise_error(Measure::CantOperateOnDifferentIngredients)
      end

      it "can't be substracted if they have different ingredients" do
        measure1 = Measure.for(ingredient: "ingredient 1", quantity: 1, unit: "unit")
        measure2 = Measure.for(ingredient: "ingredient 2", quantity: 1, unit: "unit")

        expect {
          measure1 - measure2
        }.to raise_error(Measure::CantOperateOnDifferentIngredients)
      end

      context "with same ingredient" do

        it "what happens when the result of adding or substracting is negative?"

        it "can be added when they have the same units" do
          measure1 = Measure.for(ingredient: "ingredient", quantity: 1)
          measure2 = Measure.for(ingredient: "ingredient", quantity: 2)

          measure = measure1 + measure2

          expect(measure.ingredient?("ingredient")).to eql(true)
          expect(measure.quantity?(3)).to eql(true)
          expect(measure.unit?(Measure::NoUnit)).to eql(true)
        end

        it "can be substracted when they have the same units" do
          measure1 = Measure.for(ingredient: "ingredient", quantity: 3)
          measure2 = Measure.for(ingredient: "ingredient", quantity: 2)

          measure = measure1 - measure2

          expect(measure.ingredient?("ingredient")).to eql(true)
          expect(measure.quantity?(1)).to eql(true)
          expect(measure.unit?(Measure::NoUnit)).to eql(true)
        end

        it "can be added when they have no quantity" do
          measure1 = Measure.for(ingredient: "ingredient")
          measure2 = Measure.for(ingredient: "ingredient")

          measure = measure1 + measure2

          expect(measure.ingredient?("ingredient")).to eql(true)
          expect(measure.quantity?(Quantity.no_quantity)).to eql(true)
          expect(measure.unit?(Measure::NoUnit)).to eql(true)
        end

        it "can be substracted when they have no quantity" do
          measure1 = Measure.for(ingredient: "ingredient")
          measure2 = Measure.for(ingredient: "ingredient")

          measure = measure1 - measure2

          expect(measure.ingredient?("ingredient")).to eql(true)
          expect(measure.quantity?(Quantity.no_quantity)).to eql(true)
          expect(measure.unit?(Measure::NoUnit)).to eql(true)
        end

        it "can be added when they have different units"
        it "can be substracted when they have different units"
      end

    end
  end

end
