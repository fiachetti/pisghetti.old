require "spec_helper"
require "menu"
require "recipe"
require "measure"
require "shopping_list"

class MenuesSpecFactory

  def empty_menu
    Menu.new(title: menu_title_1)
  end

  def empty_recipe_1
    @empty_recipe_1 ||= Recipe.new(title: recipe_title_1)
  end

  def empty_recipe_2
    @empty_recipe_2 ||= Recipe.new(title: recipe_title_2)
  end

  def recipe_with_ingredient_1
    @recipe_with_ingredient_1 ||=
      Recipe.new(title: recipe_title_1).tap { |recipe|
      recipe.add_measure(ingredient: ingredient_1)
    }
  end

  def recipe_with_ingredient_2
    @recipe_with_ingredient_2 ||=
      Recipe.new(title: recipe_title_2).tap { |recipe|
      recipe.add_measure(ingredient: ingredient_2)
    }
  end

  def recipe_with_two_ingredients
    @recipe_with_ingredient_1 ||=
      Recipe.new(title: recipe_title_1).tap { |recipe|
      recipe.add_measure(ingredient: ingredient_1)
      recipe.add_measure(ingredient: ingredient_2)
    }
  end

  def menu_title_1
    "Week one"
  end

  def menu_title_2
    "Week two"
  end

  def recipe_title_1
    "Lemon Panko Crusted Salmon"
  end

  def recipe_title_2
    "Roasted Beef"
  end

  def ingredient_1
    "salmon"
  end

  def ingredient_2
    "beef"
  end
end

RSpec.describe "menues" do
  let(:factory) { MenuesSpecFactory.new }

  describe "creating menues" do
    it "can't create a menu without a title" do
      expect {
        Menu.new
      }.to raise_error(ArgumentError, "missing keyword: :title")
    end

    it "can't create a menu with a blank title" do
      expect {
        Menu.new(title: nil)
      }.to raise_error(ArgumentError, "blank keyword: :title")
    end

    it "can create a menu with a valid title" do
      menu = Menu.new(title: factory.menu_title_1)

      expect(menu.empty?).to eql(true)
      expect(menu.recipe_count).to eql(0)
      expect(menu.titled?(factory.menu_title_1)).to eql(true)
      expect(menu.titled?(factory.menu_title_2)).to eql(false)
    end

  end

  describe "recipes" do
    it "can't add a recipe without a recipe" do
      menu = factory.empty_menu

      expect {
        menu.add_recipe
      }.to raise_error(ArgumentError, "missing keyword: :recipe")
      expect(menu.empty?).to eql(true)
      expect(menu.recipe_count).to eql(0)
    end

    it "can't add a blank recipe" do
      menu = factory.empty_menu

      expect {
        menu.add_recipe(recipe: "")
      }.to raise_error(ArgumentError, "blank keyword: :recipe")
      expect(menu.empty?).to eql(true)
      expect(menu.recipe_count).to eql(0)
    end

    it "can add more than one recipe to a menu" do
      menu = factory.empty_menu

      menu.add_recipe(recipe: factory.empty_recipe_1)
      menu.add_recipe(recipe: factory.empty_recipe_2)

      expect(menu.has_recipe?(factory.recipe_title_1)).to eql(true)
      expect(menu.has_recipe?(factory.recipe_title_2)).to eql(true)
      expect(menu.recipe_count).to eql(2)
    end

    it "can't remove a recipe without indicating a recipe title" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.empty_recipe_1)

      expect {
        menu.remove_recipe
      }.to raise_error(ArgumentError, "missing keyword: :title")
    end

    it "can't remove a recipe with a blank recipe title" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.empty_recipe_1)

      expect {
        menu.remove_recipe(title: "")
      }.to raise_error(ArgumentError, "blank keyword: :title")
    end

    it "can remove a recipe from a menu" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.empty_recipe_1)
      menu.add_recipe(recipe: factory.empty_recipe_2)

      menu.remove_recipe(title: factory.recipe_title_1)

      expect(menu.has_recipe?(factory.recipe_title_1)).to eql(false)
      expect(menu.has_recipe?(factory.recipe_title_2)).to eql(true)
      expect(menu.recipe_count).to eql(1)
    end

    it "can list a menu's recipes" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.empty_recipe_1)
      menu.add_recipe(recipe: factory.empty_recipe_2)

      expect(menu.recipes).to eq([
                                   factory.empty_recipe_1,
                                   factory.empty_recipe_2,
                                 ])
    end
  end

  describe "shopping list" do
    it "is empty if it has no recipes" do
      shopping_list = factory.empty_menu.shopping_list
      expect(shopping_list.empty?).to eql(true)
      expect(shopping_list.measures.empty?).to eql(true)
    end

    it "calculates the shopping list for one recipe with one ingredient" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.recipe_with_ingredient_1)

      shopping_list = menu.shopping_list
      expect(shopping_list.has_ingredient?(factory.ingredient_1)).to eql(true)
      expect(shopping_list.empty?).to eql(false)
      expect(shopping_list.measure_count).to eql(1)
      expect(shopping_list.measures).to eq([Measure.for(ingredient: factory.ingredient_1, quantity: 1)])
    end

    it "calculates the shopping list for one recipe with other ingredient" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.recipe_with_ingredient_2)

      shopping_list = menu.shopping_list
      expect(shopping_list.has_ingredient?(factory.ingredient_1)).to eql(false)
      expect(shopping_list.has_ingredient?(factory.ingredient_2)).to eql(true)
      expect(shopping_list.measures).to eq([Measure.for(ingredient: factory.ingredient_2, quantity: 1)])
    end

    it "calculates the shopping list for one recipe with more than one ingredient" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.recipe_with_two_ingredients)

      shopping_list = menu.shopping_list
      expect(shopping_list.has_ingredient?(factory.ingredient_1)).to eql(true)
      expect(shopping_list.has_ingredient?(factory.ingredient_2)).to eql(true)
      expect(shopping_list.empty?).to eql(false)
      expect(shopping_list.measure_count).to eql(2)
      expect(shopping_list.measures[0]).to eq(Measure.for(ingredient: factory.ingredient_1, quantity: 1))
      expect(shopping_list.measures[1]).to eq(Measure.for(ingredient: factory.ingredient_2, quantity: 1))
    end

    it "calculates the shopping list for one recipe with a repeated ingredient" do
      menu = factory.empty_menu
      menu.add_recipe(recipe: factory.recipe_with_two_ingredients)
      menu.add_recipe(recipe: factory.recipe_with_two_ingredients)

      shopping_list = menu.shopping_list
      expect(shopping_list.has_ingredient?(factory.ingredient_1)).to eql(true)
      expect(shopping_list.measures.first).to eq(Measure.for(ingredient: factory.ingredient_1, quantity: 2))

    end


  end
end
