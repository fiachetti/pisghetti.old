require "spec_helper"

class RecipeSystemSpecFactory
  def recipe_title_1
    "Lemon Panko Crusted Salmon"
  end

  def recipe_slug_1
    "lemon-panko-crusted-salmon"
  end

  def recipe_title_2
    "Roasted Salmon"
  end

  def valid_ingredient_1
    "salmon"
  end

  def valid_ingredient_2
    "lemon"
  end
end

RSpec.describe "recipes system" do
  let(:system) { ENVIRONMENT.recipe_system }
  let(:f) { RecipeSystemSpecFactory.new }

  if ENVIRONMENT.sequel?
    before do
      DatabaseCleaner[:sequel].start
      DatabaseCleaner[:sequel].clean
    end
  end

  it "starts with no recipes" do
    expect(system.recipes.empty?).to eql(true)
    expect(system.has_recipes?).to eql(false)
    expect(system.number_of_recipes).to eql(0)
  end

  it "can't create a recipe with an empty title" do
    expect {
      system.create_recipe(slug: f.recipe_slug_1, title: nil)
    }.to raise_error(ArgumentError, "blank keyword: :title")
  end

  it "can't create a recipe without a title" do
    expect {
      system.create_recipe(slug: f.recipe_slug_1)
    }.to raise_error(ArgumentError, "missing keyword: :title")
  end

  it "can't create a recipe with an empty slug" do
    expect {
      system.create_recipe(title: f.recipe_title_1, slug: nil)
    }.to raise_error(ArgumentError, "blank keyword: :slug")
  end

  it "can't create a recipe without a slug" do
    expect {
      system.create_recipe(title: f.recipe_title_1)
    }.to raise_error(ArgumentError, "missing keyword: :slug")
  end

  it "can create a recipe passing title and slug" do
    system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

    recipes = system.recipes
    expect(system.has_recipes?).to eql(true)
    expect(system.number_of_recipes).to eql(1)
    expect(recipes.empty?).to eql(false)
    recipe = recipes.first
    expect(recipe.title).to eql(f.recipe_title_1)
    expect(recipe.slug).to eql(f.recipe_slug_1)
    expect(recipe.measures).to be_empty
  end

  describe "measures" do
    it "can't add a measure without an ingredient" do
      system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

      expect {
        system.add_measure(slug: f.recipe_slug_1, ingredient: "")
      }.to raise_error(ArgumentError, "blank keyword: :ingredient")
    end

    it "can't add a measure without specifying the recipe slug" do
      system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

      expect {
        system.add_measure(slug: nil, ingredient: "Coco rallado")
      }.to raise_error(ArgumentError, "blank keyword: :slug")
    end

    it "can't add a measure to a non-existent recipe" do
      expect {
        system.add_measure(slug: f.recipe_slug_1, ingredient: "Coco rallado")
      }.to raise_error(RecipeSystem::NonExistentRecipe)
    end

    it "can add a measure with an ingredient" do
      system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

      system.add_measure(slug: f.recipe_slug_1, ingredient: "Coco Rallado")

      recipe = system.find_recipe(f.recipe_slug_1)

      expect(recipe.has_ingredient?("Coco Rallado")).to eql(true)
      expect(recipe.measures.map(&:ingredient)).to include("Coco Rallado")
    end

    describe "reset" do
      it "can remove all recipes" do
        system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

        system.add_measure(slug: f.recipe_slug_1, ingredient: "palta", quantity: 2)

        system.reset!

        expect(system.recipes.empty?).to eql(true)
        expect(system.has_recipes?).to eql(false)
        expect(system.number_of_recipes).to eql(0)
      end
    end

    describe "quantity" do
      it "can create a measure with an empty quantity" do
        system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

        system.add_measure(slug: f.recipe_slug_1, ingredient: "palta", quantity: "")

        recipe = system.find_recipe(f.recipe_slug_1)
        measure = recipe.measures.first
        expect(recipe.measures.count).to eql(1)
        expect(measure.ingredient?("palta")).to eql(true)
        expect(measure.quantity?(0)).to eq(true)
      end

      it "can't create a measure with an alphanumeric quantity" do
        system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

        expect {
          system.add_measure(slug: f.recipe_slug_1, ingredient: "palta", quantity: "a")
        }.to raise_error(ArgumentError, "invalid value for Integer(): \"a\"")
      end

      it "can't create a measure with a negative quantity" do
        system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

        expect {
          system.add_measure(slug: f.recipe_slug_1, ingredient: "palta", quantity: -1)
        }.to raise_error(Quantity::NegativeQuantityError)
      end

      it "defaults to no quantity" do
        system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

        system.add_measure(slug: f.recipe_slug_1, ingredient: "palta")

        recipe = system.find_recipe(f.recipe_slug_1)

        measure = recipe.measures.first
        expect(recipe.measures.count).to eql(1)
        expect(measure.ingredient?("palta")).to eql(true)
        expect(measure.quantity?(0)).to eq(true)
      end

      it "can add a measure with an ingredient and a quantity" do
        system.create_recipe(title: f.recipe_title_1, slug: f.recipe_slug_1)

        system.add_measure(slug: f.recipe_slug_1, ingredient: "palta", quantity: 2)

        recipe = system.find_recipe(f.recipe_slug_1)

        measure = recipe.measures.first
        expect(recipe.measures.count).to eql(1)
        expect(measure.ingredient?("palta")).to eql(true)
        expect(measure.quantity?(2)).to eql(true)
      end

    end
  end
end
