require "spec_helper"
require "recipe"
require "measure"

class RecipesSpecFactory
  def valid_recipe
    Recipe.new(title: recipe_title_1)
  end

  def recipe_title_1
    "Lemon Panko Crusted Salmon"
  end

  def recipe_title_2
    "Roasted Salmon"
  end

  def valid_ingredient_1
    "salmon"
  end

  def valid_ingredient_2
    "lemon"
  end
end

RSpec.describe "recipes" do
  let(:factory) { RecipesSpecFactory.new }

  describe "creating recipes" do
    it "can't create a recipe without a title" do
      expect {
        Recipe.new
      }.to raise_error(ArgumentError, "missing keyword: :title")
    end

    it "can't create a recipe with an empty title" do
      expect {
        Recipe.new(title: nil)
      }.to raise_error(Recipe::EmptyTitleError)
    end

    it "can create a recipe with a valid title" do
      recipe = Recipe.new(title: factory.recipe_title_1)

      expect(recipe.titled?(factory.recipe_title_1)).to eql(true)
      expect(recipe.ingredient_count).to eql(0)
      expect(recipe.has_ingredient?(factory.valid_ingredient_1)).to eql(false)
    end

    it "can create a recipe with a different title" do
      recipe = Recipe.new(title: factory.recipe_title_2)

      expect(recipe.titled?(factory.recipe_title_2)).to eql(true)
      expect(recipe.titled?(factory.recipe_title_1)).to eql(false)
      expect(recipe.ingredient_count).to eql(0)
      expect(recipe.has_ingredient?(factory.valid_ingredient_1)).to eql(false)
    end

  end

  describe "measures" do
    it "can't add a measure without an ingredient" do
      recipe = factory.valid_recipe

      expect {
        recipe.add_measure
      }.to raise_error(ArgumentError, "missing keyword: :ingredient")
    end

    it "can't add a measure with a blank ingredient" do
      recipe = factory.valid_recipe

      expect {
        recipe.add_measure(ingredient: nil)
      }.to raise_error(ArgumentError, "blank keyword: :ingredient")
    end

    it "can add an ingredient to a recipe" do
      recipe = factory.valid_recipe

      recipe.add_measure(ingredient: factory.valid_ingredient_1)

      expect(recipe.has_ingredient?(factory.valid_ingredient_1)).to eql(true)
      expect(recipe.ingredient_count).to eql(1)
      expect(recipe.ingredient_quantity(factory.valid_ingredient_1)).to eq(Quantity[1])
      expect(recipe.ingredient_quantity(factory.valid_ingredient_2)).to eq(Quantity[0])
    end

    it "can add more than one ingredient to a recipe" do
      recipe = factory.valid_recipe

      recipe.add_measure(ingredient: factory.valid_ingredient_1)
      recipe.add_measure(ingredient: factory.valid_ingredient_2)

      expect(recipe.has_ingredient?(factory.valid_ingredient_1)).to eql(true)
      expect(recipe.has_ingredient?(factory.valid_ingredient_2)).to eql(true)
      expect(recipe.ingredient_count).to eql(2)
    end

    it "can add more than one of the same ingredient" do
      recipe = factory.valid_recipe

      recipe.add_measure(ingredient: factory.valid_ingredient_1)
      recipe.add_measure(ingredient: factory.valid_ingredient_1)

      expect(recipe.has_ingredient?(factory.valid_ingredient_1)).to eql(true)
      expect(recipe.ingredient_quantity(factory.valid_ingredient_1)).to eq(Quantity[2])
      expect(recipe.ingredient_count).to eql(1)
    end

    it "can't remove a measure without indicating an ingredient" do
      recipe = factory.valid_recipe
      recipe.add_measure(ingredient: factory.valid_ingredient_1)

      expect {
        recipe.remove_measure
      }.to raise_error(ArgumentError, "missing keyword: :ingredient")
    end

    it "can't remove a measure with a blank ingredient" do
      recipe = factory.valid_recipe
      recipe.add_measure(ingredient: factory.valid_ingredient_1)

      expect {
        recipe.remove_measure(ingredient: "")
      }.to raise_error(ArgumentError, "blank keyword: :ingredient")
    end

    it "can remove a measure from a recipe" do
      recipe = factory.valid_recipe
      recipe.add_measure(ingredient: factory.valid_ingredient_1)
      recipe.add_measure(ingredient: factory.valid_ingredient_2)

      recipe.remove_measure(ingredient: factory.valid_ingredient_1)

      expect(recipe.has_ingredient?(factory.valid_ingredient_1)).to eql(false)
      expect(recipe.has_ingredient?(factory.valid_ingredient_2)).to eql(true)
      expect(recipe.ingredient_count).to eql(1)
    end

  end

end
