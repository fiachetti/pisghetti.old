require "dotenv"
Dotenv.load(".env.#{ENV.fetch("ENVIRONMENT")}")

if ENV['COVERAGE'] == 'true'
  require 'simplecov'
  SimpleCov.start do
    add_filter "/spec/"
  end
end

require "awesome_print"
AwesomePrint.defaults = {
  indent: 2,
  index:  false,
}

$:.unshift(File.expand_path("../../lib", __FILE__))

require "object"

require "environment/environment"
ENVIRONMENT = Environment.current

if ENVIRONMENT.sequel?
  require "database_cleaner/sequel"

  DatabaseCleaner[:sequel].db = Sequel.connect(ENV.fetch('DATABASE_URL'))
  DatabaseCleaner[:sequel].strategy = :truncation
end

RSpec.configure do |config|

  config.order = :random

  # config.expect_with :rspec do |expectations|
  #   expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  # end

  # config.mock_with :rspec do |mocks|
  #   mocks.verify_partial_doubles = true
  # end

  # config.shared_context_metadata_behavior = :apply_to_host_groups

end
