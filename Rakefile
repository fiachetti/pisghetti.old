$:.unshift(File.expand_path("..", __FILE__))
$:.unshift(File.expand_path("../lib", __FILE__))

require "environment/environment"

## Active Record related rake tasks
db_namespace = namespace :db do
  require "config/database_active_record"

  desc 'create the database'
  task :create do
    puts "Creating #{DB_PATH}..."
    touch DB_PATH
  end

  desc 'drop the database'
  task :drop do
    puts "Deleting #{DB_PATH}..."
    rm_f DB_PATH
  end

  desc 'migrate the database (options: VERSION=x).'
  task :migrate do
    ActiveRecord::Migrator.migrations_paths = [File.join(__dir__, 'db/migrate')]
    ActiveRecord::Migration.verbose = true
    version = ENV['VERSION'] ? ENV['VERSION'].to_i : nil
    args = [ActiveRecord::Migrator.migrations_paths, ActiveRecord::SchemaMigration]
    ActiveRecord::MigrationContext.new(*args).migrate(version)
    db_namespace["schema:dump"].invoke
  end

  desc 'Retrieves the current schema version number'
  task :version do
    puts "Current version: #{ActiveRecord::Migrator.current_version}"
  end

  desc 'populate the database with sample data'
  task :seed do
    require "#{__dir__}/db/seeds.rb"
  end

  desc 'Gives you a timestamp for your migration file name'
  task :timestamp do
    puts DateTime.now.strftime('%Y%m%d%H%M%S')
  end

  namespace :schema do
    desc 'Create a db/schema.rb file that can be portably used against any DB supported by AR'
    task :dump do
      require 'active_record/schema_dumper'
      filename = 'db/schema.rb'

      File.open(filename, "w:utf-8") do |file|
        ActiveRecord::SchemaDumper.dump(ActiveRecord::Base.connection, file)
      end
    end
  end

  private

  def DB_PATH
    ActiveRecord::Base.configurations['integration']['database']
  end
end
