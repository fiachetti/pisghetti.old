* Running the web server in development

  Make sure to have the environment set on the =.environment= file. This should be one of these strings:

  - test
  - development
  - integration

  Then you can just run

  #+begin_src sh
    rackup -o 0.0.0.0
  #+end_src

  The =-o= or =--host= flag with a =0.0.0.0= value indicates that the server can be accessed from anywhere and not just localhost. If you want to make this more restrictive, you can avoid setting this flag.

  If you want your code to be auto reloaded, you can use the =rerun= gem

  #+begin_src sh
    gem install rerun

    rerun rackup -o 0.0.0.0
  #+end_src

  Or if you're on a linux system, a more performant way is to install the =entr= package.

  In order to use =entr=, you first list all the files you want to track for reloading the code and then pipe that into the entr command.

  Here are the files and flags I use.

  #+begin_src sh
    ls **/*.rb config.ru .env* .environment | entr -c -r rackup -o 0.0.0.0
  #+end_src

  If any of this files change, the server will be restarted
  - =**/*.rb= :: all ruby files
  - =config.ru= :: the rackup file
  - =.env*= :: all the environment variables files
  - =.environment= :: the file that indicates the current environment

  Flags:
  - The =-r= flag indicanes the command to run. Everything after this flag will be used as the command (including the =-o= flag)
  - The =-c= flag will clear the console before restarting the command.

* Web app API
** Reset system


** List Recipes
   - Return:
     + All the recipes

** Create Recipe
   - Params:
     + title | string
     + slug  | string
   - Return:
     + The new Recipe

** Handle validation errors more elegantly in the UI

** Add Measure to recipe
   - Params:
     + recipe slug | string
     + ingredient  | string
     + quantity    | numeric
     + unit        | string
   - Return:
     + Recipe with slug =slug=

** List Menus
   - Return:
     + All the menues

** Create Menu
   - Params:
     + title | string
     + slug  | string
   - Return:
     + The new Menu

** Add recipe to Menu
   - Params
     + menu slug   | string
     + recipe slug | string
   - Return:
     + Menu with slug =slug=
