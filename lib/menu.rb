class Menu
  attr_reader :recipes

  def initialize(title:)
    validate_blank_keywords(title: title)

    @recipes = []

    @title = title
  end

  def empty?
    true
  end

  def titled?(potential_title)
    potential_title == @title
  end

  def shopping_list
    ShoppingList.new(@recipes)
  end

  def add_recipe(recipe:)
    validate_blank_keywords(recipe: recipe)

    @recipes << recipe
  end

  def remove_recipe(title:)
    validate_blank_keywords(title: title)

    @recipes.delete_if { |recipe|
      recipe.titled?(title)
    }
  end

  def has_recipe?(title)
    @recipes.any? { |recipe|
      recipe.titled?(title)
    }
  end

  def recipe_count
    @recipes.count
  end
end
