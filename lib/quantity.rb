class Quantity
  include Comparable

  ZeroQuantityError = Class.new(RuntimeError)
  NegativeQuantityError = Class.new(RuntimeError)

  attr_reader :value

  def self.no_quantity
    @no_quantity = NoQuantity.new
  end

  def self.[](value)
    return value if value.is_a?(Quantity)

    new_value = value.blank? ? no_quantity : Integer(value)

    return no_quantity              if new_value == 0
    raise NegativeQuantityError.new if new_value < 0


    new(Integer(new_value))
  end

  def to_int
    value
  end

  def coerce(other)
    [other, value]
  end

  def <=>(other)
    value <=> Quantity[other].value
  end

  def +(other)
    Quantity[value + Quantity[other].value]
  end

  def -(other)
    Quantity[value - Quantity[other].value]
  end

  def value?(potential_value)
    value == potential_value
  end

  def to_s
    value.to_s
  end

  def inspect
    "#{self.class}[#{value}]"
  end

  private

  def initialize(value)
    @value = value
  end

  NoQuantity = Class.new(Quantity) do
    def initialize; end
    def value; 0; end
    def <(*); false; end

    def inspect
      "#{self.class}"
    end

    def to_s
      ""
    end
  end

end
