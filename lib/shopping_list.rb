class ShoppingList

  def initialize(recipes)
    @recipes = recipes
  end

  def empty?
    @recipes.empty?
  end

  def measures
    @recipes
      .flat_map { |recipe| recipe.measures }
      .group_by { |measure| measure.ingredient }
      .map { |ingredient, measures|
      Measure.for(
        ingredient: ingredient,
        quantity:   measures.sum(&:quantity)
      )
    }
  end

  def has_ingredient?(potential_ingredient)
    @recipes.any? { |recipe|
      recipe.has_ingredient?(potential_ingredient)
    }
  end

  def measure_count
    @recipes.sum(&:ingredient_count)
  end

end
