class Object
  def blank?
    nil? || self =~ /\A\s*\Z/
  end

  def validate_blank_keywords(**keywords)
    keywords.each do |keyword, value|
      if value.blank?
        raise ArgumentError.new("blank keyword: :#{keyword}")
      end
    end
  end

end
