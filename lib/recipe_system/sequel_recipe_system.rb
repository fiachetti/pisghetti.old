require "recipe_system/recipe_system"
require "sequel"

class SequelRecipeSystem < RecipeSystem
  def initialize
    @db = Sequel.connect(ENV.fetch('DATABASE_URL'))
  end

  def create_recipe(slug:, title:)
    validate_blank_keywords(slug: slug, title: title)

    db[:recipes].insert(slug: slug, title: title)
  end

  def recipes
    db[:recipes].map { |attrs|
      present_recipe(slug: attrs.fetch(:slug))
    }
  end

  def add_measure(slug:, ingredient:, quantity: Quantity.no_quantity)
    validate_blank_keywords(slug: slug,
                            ingredient: ingredient)

    recipe_id = recipe_id_from_slug(slug)

    db[:measures].insert(recipe_id: recipe_id, ingredient: ingredient, quantity: Quantity[quantity].value)

    get_recipe(slug)
  end

  def reset!
    db[:recipes].delete
  end

  private

  attr_reader :db

  def recipe_id_from_slug(slug)
    recipe_hash = db[:recipes].where(slug: slug).first

    raise NonExistentRecipe.new unless recipe_hash

    recipe_hash[:id]
  end

  def get_recipe(slug)
    recipe_hash = db[:recipes].where(slug: slug).first

    raise NonExistentRecipe.new unless recipe_hash

    Recipe.new(id: recipe_hash[:id], title: recipe_hash.fetch(:title))
      .tap { |recipe|
      measure_hashes = db[:measures].where(recipe_id: recipe.id)

      measure_hashes.each do |measure_hash|
        recipe.add_measure(
          ingredient: measure_hash.fetch(:ingredient),
          quantity:   measure_hash.fetch(:quantity),
        )
      end
    }
  end

end
