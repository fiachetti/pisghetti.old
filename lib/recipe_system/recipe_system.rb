require "recipe"
require "presentation/recipe"

class RecipeSystem
  EmptySlugError = Class.new(RuntimeError)
  NonExistentRecipe = Class.new(RuntimeError)

  def reset
  end

  def create_recipe(slug:, title:)
    raise NotImplementedError
  end

  def recipes
    raise NotImplementedError
  end

  def reset!
    raise NotImplementedError
  end

  def find_recipe(slug)
    present_recipe(slug: slug)
  end

  def has_recipes?
    number_of_recipes > 0
  end

  def number_of_recipes
    recipes.count
  end

  def add_measure(slug:, ingredient:, quantity: Quantity.no_quantity)
    validate_blank_keywords(slug: slug,
                            ingredient: ingredient)

    get_recipe(slug)
      .add_measure(ingredient: ingredient, quantity: quantity)
  end

  private

  def get_recipe(slug)
    raise NotImplementedError
  end

  def present_recipe(slug:)
    Presentation::Recipe.new(
      slug: slug,
      recipe: get_recipe(slug)
    )
  end

end
