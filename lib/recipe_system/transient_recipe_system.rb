require "recipe_system/recipe_system"

class TransientRecipeSystem < RecipeSystem
  def initialize
    @recipes = {}
  end

  def recipes
    @recipes.map { |slug, recipe|
      present_recipe(slug: slug)
    }
  end

  def number_of_recipes
    @recipes.count
  end

  def has_recipes?
    @recipes.any?
  end

  def create_recipe(slug:, title:)
    validate_blank_keywords(slug: slug, title: title)

    @recipes[slug] = Recipe.new(title: title)
  end

  def reset!
    @recipes = {}
  end

  private

  def get_recipe(slug)
    @recipes[slug].tap { |recipe|
      raise NonExistentRecipe.new unless recipe
    }
  end

end
