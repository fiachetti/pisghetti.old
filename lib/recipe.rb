require "measure"

class Recipe
  EmptyTitleError = Class.new(RuntimeError)

  attr_reader :id
  attr_reader :title
  attr_reader :measures

  def initialize(title:, id: nil)
    raise EmptyTitleError if title.to_s =~ /\A\s*\Z/

    @measures = []

    @id    = id
    @title = title
  end

  def titled?(potential_title)
    @title == potential_title
  end

  def add_measure(ingredient:, quantity: Quantity[1])
    validate_blank_keywords(ingredient: ingredient)

    measure = find_ingredient(ingredient)
    @measures.delete(measure)

    @measures << Measure.for(ingredient: ingredient, quantity: measure.quantity + quantity)
  end

  def remove_measure(ingredient:)
    validate_blank_keywords(ingredient: ingredient)

    @measures.delete_if { |measure|
      measure.ingredient?(ingredient)
    }
  end

  def has_ingredient?(ingredient)
    find_ingredient(ingredient).ingredient?(ingredient)
  end

  def ingredient_quantity(ingredient)
    find_ingredient(ingredient).quantity
  end

  def ingredient_count
    @measures.count
  end

  private

  def find_ingredient(ingredient)
    @measures.find { |measure|
      measure.ingredient?(ingredient)
    } || Measure::NoMeasure.new
  end

end
