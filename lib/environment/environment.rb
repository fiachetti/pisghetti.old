require "track_descendants"

class Environment
  extend TrackDescendants

  def self.current
    descendants.find(&:current?).new
  end

  name_regexp = /\A(.+)_environment\.rb\Z/
  Dir[File.join('./lib/environment/**/*.rb')]
    .grep(name_regexp)
    .flat_map { |file_name| File.basename(file_name).scan(name_regexp).first }
    .each do |env_name|
    define_method("if_#{env_name}") do |&block|
      block.call if ENV.fetch("ENVIRONMENT") == env_name
    end
  end

  def sequel?
    false
  end
end

Dir[File.join(File.expand_path(File.join(__FILE__, '../**/*.rb')))]
  .each { |f| require f }
