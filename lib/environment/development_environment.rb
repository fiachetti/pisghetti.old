require "recipe_system/sequel_recipe_system"

class DevelopmentEnvironment < Environment
  def self.current?
    ENV['ENVIRONMENT'] == 'development'
  end

  def recipe_system
    SequelRecipeSystem.new
  end

  def sequel?
    true
  end
end
