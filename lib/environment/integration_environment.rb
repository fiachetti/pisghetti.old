require "recipe_system/sequel_recipe_system"

class IntegrationEnvironment < Environment
  def self.current?
    ENV.fetch('ENVIRONMENT') == 'integration'
  end

  def recipe_system
    SequelRecipeSystem.new
  end

  def sequel?
    true
  end
end
