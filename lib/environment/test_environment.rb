require "recipe_system/transient_recipe_system"

class TestEnvironment < Environment
  def self.current?
    ENV.fetch('ENVIRONMENT') == 'test'
  end

  def recipe_system
    TransientRecipeSystem.new
  end

end
