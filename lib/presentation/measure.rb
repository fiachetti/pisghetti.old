module Presentation
  class Measure
    def initialize(ingredient:, quantity:)
      @measure = ::Measure.for(ingredient: ingredient, quantity: quantity)
    end

    def ingredient
      @measure.ingredient
    end

    def quantity
      @measure.quantity
    end

    def ingredient?(potential_ingredient)
      @measure.ingredient?(potential_ingredient)
    end

    def quantity?(potential_quantity)
      @measure.quantity?(potential_quantity)
    end

  end
end
