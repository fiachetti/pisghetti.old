require "presentation/measure"

module Presentation
  class Recipe
    attr_reader :title
    attr_reader :slug

    def initialize(recipe:, slug:)
      @slug  = slug

      @recipe = recipe
    end

    def title
      @recipe.title
    end

    def measures
      present_measures
    end

    def has_ingredient?(ingredient)
      @recipe.has_ingredient?(ingredient)
    end

    private

    def present_measures
      @recipe.measures.map { |measure|
        Presentation::Measure.new(
          ingredient: measure.ingredient,
          quantity:   measure.quantity,
        )
      }
    end
  end
end
