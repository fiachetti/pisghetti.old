require "quantity"

class Measure
  CantOperateOnDifferentIngredients = Class.new(RuntimeError)

  attr_reader :ingredient
  attr_reader :quantity

  NoUnit     = Object.new

  def self.for(ingredient:, quantity: Quantity.no_quantity, unit: NoUnit)
    validate_blank_keywords(ingredient: ingredient)

    new(ingredient: ingredient, quantity: Quantity[quantity], unit: unit)
  end


  def ingredient?(potential_ingredient)
    potential_ingredient == @ingredient
  end

  def quantity?(potential_quantity)
    potential_quantity == @quantity
  end

  def unit?(potential_unit)
    potential_unit == @unit
  end

  def ==(other_measure)
    other_measure.ingredient?(@ingredient)  &&
      other_measure.quantity?(@quantity) &&
      other_measure.unit?(@unit)
  end

  def +(other_measure)
    algebraic_operation(:+, other_measure)
  end

  def -(other_measure)
    algebraic_operation(:-, other_measure)
  end

  private

  def initialize(ingredient:, quantity:, unit:)
    @ingredient = ingredient
    @quantity   = quantity
    @unit       = unit
  end

  def algebraic_operation(operation, other_measure)
    raise CantOperateOnDifferentIngredients.new unless other_measure.ingredient?(@ingredient)

    Measure.for(
      ingredient: @ingredient,
      quantity: @quantity.send(operation, other_measure.quantity)
    )
  end

  class NoMeasure
    def quantity
      Quantity.no_quantity
    end

    def ingredient?(ingredient)
      false
    end
  end

end
