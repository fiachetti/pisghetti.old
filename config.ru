ENV["ENVIRONMENT"] = File.read('./.environment').chomp

require "dotenv"
Dotenv.load(".env.#{ENV.fetch("ENVIRONMENT")}")

$:.unshift(File.expand_path("../lib", __FILE__))
$:.unshift(File.expand_path("../web_app", __FILE__))

require "roda"
require "app"

run App
